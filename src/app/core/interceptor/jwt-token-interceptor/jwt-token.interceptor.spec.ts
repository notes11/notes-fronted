import {TestBed} from "@angular/core/testing";

import {JwtTokenInterceptor} from "./jwt-token.interceptor";
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {
  AppInfoBackendVersionQueryHandlerService
} from "../../../backend/app-info/cqrs/query/app-info-backend-version-query-handler/app-info-backend-version-query-handler.service";
import {UserSessionService} from "../../service/user-session/user-session.service";
import {user} from "../../../../test/data/test-data.spec";

describe("JwtTokenInterceptor", () => {
  let interceptor: JwtTokenInterceptor;
  let httpMock: HttpTestingController;
  let userSession: UserSessionService;
  let appVersionHandlerService: AppInfoBackendVersionQueryHandlerService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        JwtTokenInterceptor,
        {provide: HTTP_INTERCEPTORS, useClass: JwtTokenInterceptor, multi: true}
      ]
    });
    userSession = TestBed.inject(UserSessionService);
    httpMock = TestBed.inject(HttpTestingController);
    interceptor = TestBed.inject(JwtTokenInterceptor);
    appVersionHandlerService = TestBed.inject(AppInfoBackendVersionQueryHandlerService);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it("should be created", () => {
    expect(interceptor).toBeTruthy();
  });

  it("should add Authorization header", () => {
    // given
    userSession.login(user);

    // ToDo: Add assertions
    // when
    appVersionHandlerService.execute({}).subscribe(() => {});

    // then
    const request = httpMock.expectOne("http://localhost:8080/notes/api-v1/version");
    expect(request.request.method).toBe("GET");
    request.flush("");
  });
});
