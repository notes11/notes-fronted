import {Injectable} from "@angular/core";
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from "@angular/common/http";
import {Observable} from "rxjs";
import {UserSessionService} from "../../service/user-session/user-session.service";

@Injectable()
export class JwtTokenInterceptor implements HttpInterceptor {

  private userSession: UserSessionService;

  constructor(userSession: UserSessionService) {
    this.userSession = userSession;
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const currentLoggedUser = this.userSession.getLoggedUser();

    if (currentLoggedUser !== null && currentLoggedUser.jwtToken !== null) {
      const jwtToken: string = this.addPrefixBearerIfMissing(currentLoggedUser.jwtToken);
      request = request.clone({
        setHeaders: {
          Authorization: jwtToken
        }
      });
    }

    return next.handle(request);
  }

  private addPrefixBearerIfMissing(jwtToken: string): string {
    const bearer: string = "Bearer";
    if (jwtToken.includes(bearer)) {
      return jwtToken;
    }
    return `${bearer} ${jwtToken}`;
  }

}
