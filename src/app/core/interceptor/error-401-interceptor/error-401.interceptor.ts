import {Injectable} from "@angular/core";
import {
  HttpRequest,
  HttpHandler,
  HttpEvent
} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import {BaseInterceptor} from "../base.interceptor";
import {catchError} from "rxjs/operators";
import {UserSessionService} from "../../service/user-session/user-session.service";
import {Router} from "@angular/router";

@Injectable()
export class Error401Interceptor implements BaseInterceptor {

  constructor(private userSession: UserSessionService,
              private router: Router) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request)
      .pipe(catchError(error => {
        if (error.status === 401) {
          this.userSession.logout();
          this.router.navigateByUrl("").then();
        }
        return throwError(() => "401 Unauthorized");
      }));
  }
}
