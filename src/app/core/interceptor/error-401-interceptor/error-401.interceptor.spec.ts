import {TestBed} from "@angular/core/testing";

import {Error401Interceptor} from "./error-401.interceptor";
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {UserSessionService} from "../../service/user-session/user-session.service";
import {
  AppInfoBackendVersionQueryHandlerService
} from "../../../backend/app-info/cqrs/query/app-info-backend-version-query-handler/app-info-backend-version-query-handler.service";
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {RouterTestingModule} from "@angular/router/testing";
import {appRoutes} from "../../../routes";
import {user} from "../../../../test/data/test-data.spec";

describe("Error401Interceptor", () => {
  let interceptor: Error401Interceptor;
  let httpMock: HttpTestingController;
  let userSession: UserSessionService;
  let appVersionHandlerService: AppInfoBackendVersionQueryHandlerService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes(appRoutes)],
      providers: [
        Error401Interceptor,
        {provide:HTTP_INTERCEPTORS, useClass: Error401Interceptor, multi: true},
      ]
    });
    userSession = TestBed.inject(UserSessionService);
    interceptor = TestBed.inject(Error401Interceptor);
    httpMock = TestBed.inject(HttpTestingController);
    appVersionHandlerService = TestBed.inject(AppInfoBackendVersionQueryHandlerService);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it("should be created", () => {
    expect(interceptor).toBeTruthy();
  });

  it("should intercept 401 error", () => {
    // given
    userSession.login(user);

    // when & then
    appVersionHandlerService.execute({}).subscribe({
      next: () => {},
      error: err => {
        expect(err).toBe("401 Unauthorized");
        expect(userSession.isUserLoggedIn()).toBeFalsy();
      },
    });

    const request = httpMock.expectOne("http://localhost:8080/notes/api-v1/version");
    expect(request.request.method).toBe("GET");
    request.flush("", {
      status: 401,
      statusText: "Unauthorized"
    });
  });
});
