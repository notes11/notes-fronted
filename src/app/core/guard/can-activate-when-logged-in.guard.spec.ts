import {CanActivateWhenLoggedInGuard} from "./can-activate-when-logged-in.guard";
import {ActivatedRouteSnapshot, Router, RouterStateSnapshot} from "@angular/router";
import {UserSessionService} from "../service/user-session/user-session.service";
import {user} from "../../../test/data/test-data.spec";

export const fakeRouterState = (url: string): RouterStateSnapshot => ({url} as RouterStateSnapshot);

describe("CanActivateWhenLoggedInGuard", () => {
  let guard: CanActivateWhenLoggedInGuard;
  let router: jasmine.SpyObj<Router>;
  let userSessionService: UserSessionService;
  const dummyRoute = {} as ActivatedRouteSnapshot;

  beforeEach(() => {
    router = jasmine.createSpyObj<Router>("Router", ["navigateByUrl"]);
    userSessionService = new UserSessionService();
    guard = new CanActivateWhenLoggedInGuard(router, userSessionService);
  });

  // noinspection DuplicatedCode
  it("should be created", () => {
    expect(guard).toBeTruthy();
  });

  it("should return 'true' on 'canActivate'", () => {
    userSessionService.login(user);
    expect(guard.canActivate(dummyRoute, fakeRouterState(""))).toBeTruthy();
  });

  it("should return 'false' on 'canActivate'", () => {
    userSessionService.logout();
    expect(guard.canActivate(dummyRoute, fakeRouterState(""))).toBeFalsy();
  });
});
