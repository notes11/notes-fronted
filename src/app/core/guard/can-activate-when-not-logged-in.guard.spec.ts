import {CanActivateWhenNotLoggedInGuard} from "./can-activate-when-not-logged-in-guard.service";
import {ActivatedRouteSnapshot, Router} from "@angular/router";
import {UserSessionService} from "../service/user-session/user-session.service";
import {fakeRouterState} from "./can-activate-when-logged-in.guard.spec";
import {user} from "../../../test/data/test-data.spec";

describe("CanActiveWhenNotLoggedInGuard", () => {
  let guard: CanActivateWhenNotLoggedInGuard;
  let router: jasmine.SpyObj<Router>;
  let userSessionService: UserSessionService;
  const dummyRoute = {} as ActivatedRouteSnapshot;

  beforeEach(() => {
    router = jasmine.createSpyObj<Router>("Router", ["navigateByUrl"]);
    userSessionService = new UserSessionService();
    guard = new CanActivateWhenNotLoggedInGuard(router, userSessionService);
  });

  // noinspection DuplicatedCode
  it("should be created", () => {
    expect(guard).toBeTruthy();
  });

  it("should return 'true' on 'canActivate'", () => {
    userSessionService.logout();
    expect(guard.canActivate(dummyRoute, fakeRouterState(""))).toBeTruthy();
  });

  it("should return 'false' on 'canActivate'", () => {
    userSessionService.login(user);
    expect(guard.canActivate(dummyRoute, fakeRouterState(""))).toBeFalsy();
  });
});
