export class Configuration {
  public environment: string = "local";
  public title: string = "Notes";
  public backend: Backend = new Backend();
}

export class Backend {
  public apiBaseUri: string = "http://localhost:8080";
  public apiBasePath: string = "/notes/api-v1";
  public apiMappings: ApiMappings = new ApiMappings();
}

export class ApiMappings {
  public appInfo: AppInfo = new AppInfo();
  public login: string = "/login";
  public user: string = "/user";
  public note: string = "/note";
}

export class AppInfo {
  public version: string = "/version";
  public environment: string = "/environment";
}
