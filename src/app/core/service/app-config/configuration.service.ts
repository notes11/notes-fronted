import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Configuration} from "./configuration";
import {environment} from "../../../../environments/environment";

@Injectable({
  providedIn: "root"
})
export class ConfigurationService {

  private httpClient: HttpClient;
  private configuration: Configuration = new Configuration();

  constructor(httpClient: HttpClient) {
    this.httpClient = httpClient;
  }

  public loadConfig() {
    const urlToAppConfig = environment.urlToAppConfig;
    return new Promise(((resolve) => {
      this.httpClient.get<Configuration>(urlToAppConfig).subscribe(response => {
        this.configuration = response;
        resolve(true);
      });
    }));
  }

  public getConfiguration(): Configuration {
    return this.configuration;
  }
}
