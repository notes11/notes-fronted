import {ConfigurationService} from "./configuration.service";

export const appInitializerFunction = (configurationService: ConfigurationService) => () => configurationService.loadConfig();
