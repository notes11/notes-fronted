import {Injectable} from "@angular/core";
import {User} from "../../../backend/model/dto/user";

@Injectable({
  providedIn: "root"
})
export class UserSessionService {

  private sessionKeyName: string = "user";

  public login(user: User): void {
    this.refresh(user);
  }

  public logout(): void {
    sessionStorage.removeItem(this.sessionKeyName);
  }

  public getLoggedUser(): User {
    return JSON.parse(sessionStorage.getItem(this.sessionKeyName));
  }

  public isUserLoggedIn(): boolean {
    return sessionStorage.getItem(this.sessionKeyName) !== null;
  }

  public refresh(user: User): void {
    sessionStorage.setItem(this.sessionKeyName, JSON.stringify(user));
  }

}
