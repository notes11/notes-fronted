import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {ConfigurationService} from "./app-config/configuration.service";

@Injectable({
  providedIn: "root"
})
export abstract class BaseService {

  protected apiBaseUri: string = "";
  protected apiBasePath: string = "";
  protected httpClient: HttpClient;
  protected configurationService: ConfigurationService;

  protected constructor(httpClient: HttpClient, configurationService: ConfigurationService) {
    this.httpClient = httpClient;
    this.configurationService = configurationService;
    this.apiBaseUri = this.configurationService.getConfiguration().backend.apiBaseUri;
    this.apiBasePath = this.configurationService.getConfiguration().backend.apiBasePath;
  }

  protected get apiBaseUriWithBasePath(): string {
    return this.apiBaseUri + this.apiBasePath;
  }

}
