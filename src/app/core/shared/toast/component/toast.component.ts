import {Component} from "@angular/core";
import {Toast} from "../model/toast";
import {ToastManagerService} from "../service/toast-manager.service";

@Component({
  selector: "app-toast",
  templateUrl: "./toast.component.html",
  styleUrls: ["./toast.component.scss"],
})
export class ToastComponent {

  constructor(private toastManagerService: ToastManagerService) {
  }

  private static timeDiffInSeconds(start: Date, end: Date): number {
    return (end.getTime() - start.getTime()) / 1000;
  }

  get toasts(): Toast[] {
    return this.toastManagerService.toasts;
  }

  public remove(toast: Toast): void {
    this.toastManagerService.remove(toast);
  }

  public time(toast: Toast): string {
    if (toast && toast.time) {
      const currentDateTime: Date = new Date(Date.now());
      if (ToastComponent.timeDiffInSeconds(toast.time, currentDateTime) < 5) { // less than 5 seconds ago
        return "Just now";
      } else if (ToastComponent.timeDiffInSeconds(toast.time, currentDateTime) < 60) { // less than 60 seconds ago
        return `${ToastComponent.timeDiffInSeconds(toast.time, currentDateTime).toFixed()} seconds ago`;
      } else if (this.timeDiffInMinutes(toast.time, currentDateTime) < 60) { // less than 60 minutes ago
        return `${this.timeDiffInMinutes(toast.time, currentDateTime).toFixed()} minutes ago`;
      } else { // more than 60 min
        return `${this.timeDiffInHours(toast.time, currentDateTime).toFixed()} hours ago`;
      }
    }
    return "";
  }

  private timeDiffInMinutes(start: Date, end: Date): number {
    return ToastComponent.timeDiffInSeconds(start, end) / 60;
  }

  private timeDiffInHours(start: Date, end: Date): number {
    return this.timeDiffInMinutes(start, end) / 60;
  }

}
