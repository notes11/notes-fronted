import {ComponentFixture, fakeAsync, TestBed} from "@angular/core/testing";

import { ToastComponent } from "./toast.component";
import {ToastManagerService} from "../service/toast-manager.service";

describe("ToastComponent", () => {
  let component: ToastComponent;
  let toastManagerService: ToastManagerService;
  let fixture: ComponentFixture<ToastComponent>;
  const message: string = "Some message";

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ToastComponent ],
      providers: [ToastManagerService]
    }).compileComponents();
    toastManagerService = TestBed.inject(ToastManagerService);
    fixture = TestBed.createComponent(ToastComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should show notification '' when undefine", () => {
    expect(component.time(undefined)).toEqual("");
  });

  it("should show notification 'Just now'", () => {
    toastManagerService.showSuccess(message);
    expect(component.toasts.length).toBeGreaterThan(0);
    const toast = component.toasts[0];
    expect(component.time(toast)).toEqual("Just now");
  });

  it("should contain 'seconds ago'", fakeAsync(() => {
    toastManagerService.showSuccess(message);
    expect(component.toasts.length).toBeGreaterThan(0);
    const toast = component.toasts[0];
    jasmine.clock().tick(6000);
    expect(component.time(toast)).toContain("seconds ago");
  }));

  it("should contain 'minutes ago'", fakeAsync(() => {
    toastManagerService.showSuccess(message);
    expect(component.toasts.length).toBeGreaterThan(0);
    const toast = component.toasts[0];
    jasmine.clock().tick(60000);
    expect(component.time(toast)).toContain("minutes ago");
  }));

  it("should contain 'hours ago'", fakeAsync(() => {
    toastManagerService.showSuccess(message);
    expect(component.toasts.length).toBeGreaterThan(0);
    const toast = component.toasts[0];
    jasmine.clock().tick(3600000);
    expect(component.time(toast)).toContain(" hours ago");
  }));

  it("should remove", () => {
    toastManagerService.showSuccess(message);
    expect(component.toasts.length).toBeGreaterThan(0);
    const toast = component.toasts[0];
    component.time(toast);
    component.remove(toast);
    expect(component.toasts.length).toEqual(0);
  });
});
