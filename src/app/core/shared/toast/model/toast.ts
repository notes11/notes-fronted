export class Toast {

  constructor(public clazz?: string,
              public header?: string,
              public time?: Date,
              public body?: string,
              public autohide: boolean = true,
              public delay: number = 4000) {
  }

}
