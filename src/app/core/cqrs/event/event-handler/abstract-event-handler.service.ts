import {EventResult} from "../result/event-result";
import {Injectable} from "@angular/core";
import {BaseService} from "../../../service/base.service";
import {HttpClient} from "@angular/common/http";
import {Event} from "../event";
import {EventHandler} from "./event-handler";
import {Observable} from "rxjs";
import {ConfigurationService} from "../../../service/app-config/configuration.service";

@Injectable({
  providedIn: "root"
})
export abstract class AbstractEventHandler<E extends Event, R extends EventResult> extends BaseService
  implements EventHandler<E, R> {

  protected constructor(httpClient: HttpClient, configurationService: ConfigurationService) {
    super(httpClient, configurationService);
  }

  abstract execute(event: E): Observable<R> | R;

}
