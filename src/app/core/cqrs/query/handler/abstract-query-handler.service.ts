import {Query} from "../query";
import {QueryResult} from "../result/query-result";
import {AbstractEventHandler} from "../../event/event-handler/abstract-event-handler.service";
import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {QueryHandler} from "./query-handler";
import {ConfigurationService} from "../../../service/app-config/configuration.service";

@Injectable({
  providedIn: "root"
})
export abstract class AbstractQueryHandler<Q extends Query, R extends QueryResult> extends AbstractEventHandler<Q, R>
  implements QueryHandler<Q, R> {

  protected constructor(httpClient: HttpClient, configurationService: ConfigurationService) {
    super(httpClient, configurationService);
  }

}
