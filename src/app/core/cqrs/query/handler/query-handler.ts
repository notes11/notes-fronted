import {Query} from "../query";
import {QueryResult} from "../result/query-result";
import {EventHandler} from "../../event/event-handler/event-handler";
import {Observable} from "rxjs";

export interface QueryHandler<Q extends Query, R extends QueryResult> extends EventHandler<Q, R> {

  execute(query: Q): Observable<R> | R;

}
