import {EventResult} from "../../event/result/event-result";

export interface CommandResult extends EventResult {
}
