import {Command} from "../command";
import {CommandResult} from "../result/command-result";
import {EventHandler} from "../../event/event-handler/event-handler";
import {Observable} from "rxjs";

export interface CommandHandler<C extends Command, R extends CommandResult> extends EventHandler<C, R> {

  execute(command: C): Observable<R> | R;

}
