import {Command} from "../command";
import {CommandResult} from "../result/command-result";
import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {AbstractEventHandler} from "../../event/event-handler/abstract-event-handler.service";
import {Observable} from "rxjs";
import {CommandHandler} from "./command-handler";
import {ConfigurationService} from "../../../service/app-config/configuration.service";

@Injectable({
  providedIn: "root"
})
export abstract class AbstractCommandHandler<C extends Command, R extends CommandResult> extends AbstractEventHandler<C, R>
  implements CommandHandler<C, R> {

  protected constructor(httpClient: HttpClient, configurationService: ConfigurationService) {
    super(httpClient, configurationService);
  }

  abstract execute(command: C): Observable<R> | R;

}
