import {Routes} from "@angular/router";
import {environment} from "../environments/environment";
import {UserRegisterComponent} from "./frontend/view/user/user-register/user-register.component";
import {UserLoginComponent} from "./frontend/view/user/user-login/user-login.component";
import {StatusComponent} from "./frontend/shared-component/status/status.component";
import {MainComponent} from "./frontend/view/main/main.component";
import {CanActivateWhenNotLoggedInGuard} from "./core/guard/can-activate-when-not-logged-in-guard.service";
import {UserProfileComponent} from "./frontend/view/user/user-profile/user-profile.component";
import {CanActivateWhenLoggedInGuard} from "./core/guard/can-activate-when-logged-in.guard";
import {NoteListComponent} from "./frontend/view/note/note-list/note-list.component";
import { NoteComponent } from "./frontend/view/note/note/note.component";

export const appRoutes: Routes = [
  {
    path: "",
    component: MainComponent
  },
  {
    path: environment.appMapping.user.register,
    component: UserRegisterComponent,
    canActivate: [CanActivateWhenNotLoggedInGuard]
  },
  {
    path: environment.appMapping.user.login,
    component: UserLoginComponent,
    canActivate: [CanActivateWhenNotLoggedInGuard]
  },
  {
    path: environment.appMapping.status,
    component: StatusComponent
  },
  {
    path: environment.appMapping.user.profile,
    component: UserProfileComponent,
    canActivate: [CanActivateWhenLoggedInGuard]
  },
  {
    path: environment.appMapping.note.noteList,
    component: NoteListComponent,
    canActivate: [CanActivateWhenLoggedInGuard]
  },
  {
    path: environment.appMapping.note.note,
    component: NoteComponent,
    canActivate: [CanActivateWhenLoggedInGuard]
  },
  {
    path: environment.appMapping.note.note + "/:uuid",
    component: NoteComponent,
    canActivate: [CanActivateWhenLoggedInGuard]
  },
  // otherwise redirect to home
  {
    path: "**",
    redirectTo: ""
  }
];
