import {ComponentFixture, TestBed} from "@angular/core/testing";
import {AppComponent} from "./app.component";
import {UserIdleService} from "angular-user-idle";
import {Router} from "@angular/router";
import {UserSessionService} from "./core/service/user-session/user-session.service";
import {ToastManagerService} from "./core/shared/toast/service/toast-manager.service";
import {RouterTestingModule} from "@angular/router/testing";
import {DebugElement} from "@angular/core";

describe("AppComponent", () => {
  let userIdle: UserIdleService;
  let router: Router;
  let userSession: UserSessionService;
  let toastManager: ToastManagerService;
  let fixture: ComponentFixture<AppComponent>;
  let appComponent: AppComponent;
  let debugElement: DebugElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [
        AppComponent
      ],
    }).compileComponents();
    userIdle = TestBed.inject(UserIdleService);
    router = TestBed.inject(Router);
    userSession = TestBed.inject(UserSessionService);
    toastManager = TestBed.inject(ToastManagerService);
    fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    appComponent = fixture.componentInstance;
    debugElement = fixture.debugElement;
  });

  it("should create the app", () => {
    expect(appComponent).toBeTruthy();
  });
});
