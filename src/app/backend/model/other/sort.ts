import { Params } from "@angular/router";
import { Direction } from "./direction";

export class Sort {

  public direction: Direction = Direction.DESC;

  constructor(private params?: Params) {
    if (params && (params.direction ===  Direction.DESC || params.direction ===  Direction.ASC)) {
      this.direction = params.direction;
    }
  }

}


