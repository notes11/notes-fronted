export class Page<T> {

  public content: T[] = [];
  public last: boolean;
  public totalPages: number;
  public totalElements: number;
  public first: boolean;
  public numberOfElements: number;
  public size: number;
  // eslint-disable-next-line id-blacklist
  public number: number;

}
