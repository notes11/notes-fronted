import {BaseInfo} from "./base-info";
import {User} from "./user";

export class Note extends BaseInfo {

  public title: string;
  public noteText: string;
  public owner: User;

}
