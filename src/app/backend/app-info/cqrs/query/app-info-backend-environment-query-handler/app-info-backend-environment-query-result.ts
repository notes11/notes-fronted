import {QueryResult} from "../../../../../core/cqrs/query/result/query-result";

export class AppInfoBackendEnvironmentQueryResult implements QueryResult {
  public environment: string;
}
