import {Injectable} from "@angular/core";
import {AbstractQueryHandler} from "../../../../../core/cqrs/query/handler/abstract-query-handler.service";
import {AppInfoBackendEnvironmentQuery} from "./app-info-backend-environment-query";
import {AppInfoBackendEnvironmentQueryResult} from "./app-info-backend-environment-query-result";
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {ConfigurationService} from "../../../../../core/service/app-config/configuration.service";

@Injectable({
  providedIn: "root"
})
export class AppInfoBackendEnvironmentHandlerService
  extends AbstractQueryHandler<AppInfoBackendEnvironmentQuery, AppInfoBackendEnvironmentQueryResult> {

  private readonly environmentUrl: string;

  constructor(httpClient: HttpClient, configurationService: ConfigurationService) {
    super(httpClient, configurationService);
    // eslint-disable-next-line
    this.environmentUrl = `${this.apiBaseUriWithBasePath}${this.configurationService.getConfiguration().backend.apiMappings.appInfo.environment}`;
  }

  execute(query: AppInfoBackendEnvironmentQuery): Observable<AppInfoBackendEnvironmentQueryResult> {
    return this.httpClient.get<AppInfoBackendEnvironmentQueryResult>(this.environmentUrl);
  }
}
