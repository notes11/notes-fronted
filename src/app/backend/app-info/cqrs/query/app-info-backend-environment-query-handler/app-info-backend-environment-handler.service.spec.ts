import {TestBed} from "@angular/core/testing";

import {AppInfoBackendEnvironmentHandlerService} from "./app-info-backend-environment-handler.service";
import {HttpClientTestingModule} from "@angular/common/http/testing";

describe("AppInfoBackendEnvironmentHandlerService", () => {
  let service: AppInfoBackendEnvironmentHandlerService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(AppInfoBackendEnvironmentHandlerService);
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });
});
