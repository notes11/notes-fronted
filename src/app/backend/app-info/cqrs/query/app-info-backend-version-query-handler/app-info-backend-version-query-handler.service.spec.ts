import {TestBed} from "@angular/core/testing";

import {AppInfoBackendVersionQueryHandlerService} from "./app-info-backend-version-query-handler.service";
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {AppInfoBackendVersionQuery} from "./app-info-backend-version-query";
import {AppInfoBackendVersionQueryResult} from "./app-info-backend-version-query-result";
import {ConfigurationService} from "../../../../../core/service/app-config/configuration.service";

describe("AppInfoBackendVersionQueryHandlerService", () => {
  let service: AppInfoBackendVersionQueryHandlerService;
  let httpMock: HttpTestingController;

  let configurationService: ConfigurationService;
  let versionUrl: string;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    configurationService = TestBed.inject(ConfigurationService);
    versionUrl = "";
    versionUrl += configurationService.getConfiguration().backend.apiBaseUri;
    versionUrl += configurationService.getConfiguration().backend.apiBasePath;
    versionUrl += configurationService.getConfiguration().backend.apiMappings.appInfo.version;

    httpMock = TestBed.inject(HttpTestingController);
    service = TestBed.inject(AppInfoBackendVersionQueryHandlerService);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });

  it("should return version", () => {
    // given
    const backendVersionResult: AppInfoBackendVersionQueryResult = {version: "1.0.0"};

    // when & then
    expect(service.execute(new AppInfoBackendVersionQuery()).subscribe(result => {
      expect(result.version).toBeTruthy();
      expect(result.version).toEqual(backendVersionResult.version);
    }));

    const request = httpMock.expectOne(versionUrl);
    expect(request.request.method).toBe("GET");
    request.flush(backendVersionResult);
  });
});
