import { HttpClient, HttpParams } from "@angular/common/http";
import { AbstractQueryHandler } from "src/app/core/cqrs/query/handler/abstract-query-handler.service";
import { QueryResult } from "src/app/core/cqrs/query/result/query-result";
import { ConfigurationService } from "src/app/core/service/app-config/configuration.service";
import { BaseQuery } from "./base-query";

export abstract class BaseQueryHandler<Q extends BaseQuery, R extends QueryResult> extends AbstractQueryHandler<Q, R> {

  protected constructor(httpClient: HttpClient, configurationService: ConfigurationService) {
    super(httpClient, configurationService);
  }

  protected baseHttpParams(query?: Q): HttpParams {
    let httpParams: HttpParams = new HttpParams();

    if (query) {
      httpParams = httpParams.append("size", query.size);
      httpParams = httpParams.append("page", query.page);
      httpParams = httpParams.append("direction", query.direction.toString());

      if (query.uuid) {
        httpParams = httpParams.append("uuid", query.uuid);
      }

      if (query.createdBy) {
        httpParams = httpParams.append("createdBy", query.createdBy);
      }
      if (query.createdTimestampAfter) {
        httpParams = httpParams.append("createdTimestampAfter", new Date(query.createdTimestampAfter).toISOString());
      }
      if (query.createdTimestampBefore) {
        httpParams = httpParams.append("createdTimestampBefore", new Date(query.createdTimestampBefore).toISOString());
      }

      if (query.modifiedBy) {
        httpParams = httpParams.append("modifiedBy", query.modifiedBy);
      }
      if (query.modifiedTimestampAfter) {
        httpParams = httpParams.append("modifiedTimestampAfter", new Date(query.modifiedTimestampAfter).toISOString());
      }
      if (query.modifiedTimestampBefore) {
        httpParams = httpParams.append("modifiedTimestampBefore", new Date(query.modifiedTimestampBefore).toISOString());
      }
    }

    return httpParams;
  }

}
