import {Injectable} from "@angular/core";
import {NoteQuery} from "./note-query";
import {NoteQueryResult} from "./note-query-result";
import {Observable} from "rxjs";
import {HttpClient, HttpParams} from "@angular/common/http";
import {ConfigurationService} from "../../../../../core/service/app-config/configuration.service";
import { BaseQueryHandler } from "../base-query-handler.service";

@Injectable({
  providedIn: "root"
})
export class NoteQueryHandlerService extends BaseQueryHandler<NoteQuery, NoteQueryResult> {

  private readonly noteUrl: string;

  constructor(httpClient: HttpClient, configurationService: ConfigurationService) {
    super(httpClient, configurationService);
    this.noteUrl = this.apiBaseUriWithBasePath;
    this.noteUrl += this.configurationService.getConfiguration().backend.apiMappings.note;
  }

  execute(query: NoteQuery): Observable<NoteQueryResult> {
    let httpParams: HttpParams = super.baseHttpParams(query);

    if (query.title) {
      httpParams = httpParams.append("title", query.title);
    }

    return this.httpClient.get<NoteQueryResult>(this.noteUrl, {
      params: httpParams
    });
  }
}
