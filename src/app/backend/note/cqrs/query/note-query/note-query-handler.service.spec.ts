import { DatePipe } from "@angular/common";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { TestBed } from "@angular/core/testing";

import { NoteQueryHandlerService } from "./note-query-handler.service";

describe("NoteQueryHandlerService", () => {
  let service: NoteQueryHandlerService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(NoteQueryHandlerService);
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });
});
