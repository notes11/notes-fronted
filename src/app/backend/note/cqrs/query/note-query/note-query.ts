import { Params } from "@angular/router";
import {Query} from "../../../../../core/cqrs/query/query";
import { BaseQuery } from "../base-query";

export class NoteQuery extends BaseQuery implements Query {

  public title: string;

  constructor(params?: Params) {
    super(params);

    if (params) {
      if (params.title) {
        this.title = params.title;
      }
    }
  }

}
