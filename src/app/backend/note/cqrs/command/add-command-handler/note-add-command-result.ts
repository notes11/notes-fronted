import {Command} from "../../../../../core/cqrs/command/command";
import {NoteCommandResult} from "../note-command-result";

export class NoteAddCommandResult extends NoteCommandResult implements Command {
}
