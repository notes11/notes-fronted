import {Command} from "../../../../../core/cqrs/command/command";

export class NoteDeleteCommand implements Command {

  public uuid: string;

}
