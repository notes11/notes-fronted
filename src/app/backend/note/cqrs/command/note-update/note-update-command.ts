import { NoteAddCommand } from "../add-command-handler/note-add-command";

export class NoteUpdateCommand extends NoteAddCommand {
  public uuid: string;
}
