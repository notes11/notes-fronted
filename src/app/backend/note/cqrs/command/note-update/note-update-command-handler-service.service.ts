import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { AbstractCommandHandler } from "src/app/core/cqrs/command/handler/abstract-command-handler.service";
import { ConfigurationService } from "src/app/core/service/app-config/configuration.service";
import { NoteUpdateCommand } from "./note-update-command";
import { NoteUpdateCommandResult } from "./note-update-command-result";

@Injectable({
  providedIn: "root"
})
export class NoteUpdateCommandHandlerServiceService extends AbstractCommandHandler<NoteUpdateCommand, NoteUpdateCommandResult> {

  private readonly apiBaseNotePath: string;

  constructor(httpClient: HttpClient, configurationService: ConfigurationService) {
    super(httpClient, configurationService);
    this.apiBaseNotePath = this.configurationService.getConfiguration().backend.apiMappings.note;
  }

  execute(command: NoteUpdateCommand): Observable<NoteUpdateCommandResult> {
    const fullUrl: string = `${this.apiBaseUriWithBasePath }${this.apiBaseNotePath}/${command.uuid}`;
    return this.httpClient.put<NoteUpdateCommandResult>(fullUrl, {
      title : command.title,
      noteText : command.noteText
    });
  }
}
