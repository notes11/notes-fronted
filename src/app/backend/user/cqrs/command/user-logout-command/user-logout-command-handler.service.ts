import {Injectable} from "@angular/core";
import {AbstractCommandHandler} from "../../../../../core/cqrs/command/handler/abstract-command-handler.service";
import {UserLogoutCommand} from "./user-logout-command";
import {UserLogoutCommandResult} from "./user-logout-command-result";
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {UserSessionService} from "../../../../../core/service/user-session/user-session.service";
import {ConfigurationService} from "../../../../../core/service/app-config/configuration.service";

@Injectable({
  providedIn: "root"
})
export class UserLogoutCommandHandlerService extends AbstractCommandHandler<UserLogoutCommand, UserLogoutCommandResult> {

  private userSession: UserSessionService;

  constructor(httpClient: HttpClient, userSession: UserSessionService, configurationService: ConfigurationService) {
    super(httpClient, configurationService);
    this.userSession = userSession;
  }

  execute(command: UserLogoutCommand): Observable<UserLogoutCommandResult> | UserLogoutCommandResult {
    this.userSession.logout();
    return new UserLogoutCommandResult();
  }
}
