import {Command} from "../../../../../core/cqrs/command/command";

export class UserLoginCommand implements Command {

  public username: string;
  public password: string;

}
