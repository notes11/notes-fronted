import {TestBed} from "@angular/core/testing";

import {UserLoginCommandHandlerService} from "./user-login-command-handler.service";
import {UserSessionService} from "../../../../../core/service/user-session/user-session.service";
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {HttpClient} from "@angular/common/http";
import {ConfigurationService} from "../../../../../core/service/app-config/configuration.service";
import {userLoginCommand, userUpdateCommandResult} from "../../../../../../test/data/test-data.spec";
import {userExpectToBe} from "../../../../../../test/utils/test-utils.spec";

describe("UserLoginCommandHandlerService", () => {
  let service: UserLoginCommandHandlerService;
  let userSession: UserSessionService;
  let httpMock: HttpTestingController;
  let httpClient: HttpClient;

  let configurationService: ConfigurationService;
  let loginTestUrl: string;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    configurationService = TestBed.inject(ConfigurationService);
    loginTestUrl = "";
    loginTestUrl += configurationService.getConfiguration().backend.apiBaseUri;
    loginTestUrl += configurationService.getConfiguration().backend.apiBasePath;
    loginTestUrl += configurationService.getConfiguration().backend.apiMappings.login;

    httpMock = TestBed.inject(HttpTestingController);
    httpClient = TestBed.inject(HttpClient);
    userSession = TestBed.inject(UserSessionService);
    service = TestBed.inject(UserLoginCommandHandlerService);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });

  it("should be login", () => {
    service.execute(userLoginCommand)
      .subscribe(result => {
        userExpectToBe(result, userUpdateCommandResult);
      });

    const request = httpMock.expectOne(loginTestUrl);
    expect(request.request.method).toBe("POST");
    request.flush(userUpdateCommandResult);
  });
});
