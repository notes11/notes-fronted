import {TestBed} from "@angular/core/testing";

import {UserRegisterCommandHandlerService} from "./user-register-command-handler.service";
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {HttpClient} from "@angular/common/http";
import {ConfigurationService} from "../../../../../core/service/app-config/configuration.service";
import {userRegisterCommand, userRegisterCommandResult} from "../../../../../../test/data/test-data.spec";

describe("UserRegisterCommandHandlerService", () => {
  let httpMock: HttpTestingController;
  let httpClient: HttpClient;
  let service: UserRegisterCommandHandlerService;

  let configurationService: ConfigurationService;
  let registerUrl: string;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    configurationService = TestBed.inject(ConfigurationService);
    registerUrl = "";
    registerUrl += configurationService.getConfiguration().backend.apiBaseUri;
    registerUrl += configurationService.getConfiguration().backend.apiBasePath;
    registerUrl += configurationService.getConfiguration().backend.apiMappings.user;

    httpMock = TestBed.inject(HttpTestingController);
    httpClient = TestBed.inject(HttpClient);
    service = TestBed.inject(UserRegisterCommandHandlerService);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });

  it("should register a user", () => {
    service.execute(userRegisterCommand).subscribe(result => {
      expect(result).toBeTruthy();
    });

    const request = httpMock.expectOne(registerUrl);
    expect(request.request.method).toBe("POST");
    request.flush(userRegisterCommandResult);
  });
});
