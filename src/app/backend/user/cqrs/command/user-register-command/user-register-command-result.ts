import {CommandResult} from "../../../../../core/cqrs/command/result/command-result";
import {User} from "../../../../model/dto/user";

export class UserRegisterCommandResult extends User implements CommandResult {
}
