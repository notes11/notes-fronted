import {Injectable} from "@angular/core";
import {AbstractCommandHandler} from "../../../../../core/cqrs/command/handler/abstract-command-handler.service";
import {UserRegisterCommand} from "./user-register-command";
import {UserRegisterCommandResult} from "./user-register-command-result";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {ConfigurationService} from "../../../../../core/service/app-config/configuration.service";

@Injectable({
  providedIn: "root"
})
export class UserRegisterCommandHandlerService extends AbstractCommandHandler<UserRegisterCommand, UserRegisterCommandResult> {

  private readonly apiBaseUserPath: string;

  constructor(httpClient: HttpClient, configurationService: ConfigurationService) {
    super(httpClient, configurationService);
    this.apiBaseUserPath = this.configurationService.getConfiguration().backend.apiMappings.user;
  }

  public execute(command: UserRegisterCommand): Observable<UserRegisterCommandResult> {
    return this.httpClient.post<UserRegisterCommandResult>(this.apiBaseUriWithBasePath + this.apiBaseUserPath, command);
  }

}
