import {TestBed} from "@angular/core/testing";

import {UserUpdateCommandHandlerService} from "./user-update-command-handler.service";
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {ConfigurationService} from "../../../../../core/service/app-config/configuration.service";
import {userUpdateCommand, userUpdateCommandResult} from "../../../../../../test/data/test-data.spec";
import {userExpectToBe} from "../../../../../../test/utils/test-utils.spec";

describe("UpdateCommandHandlerService", () => {
  let service: UserUpdateCommandHandlerService;
  let httpMock: HttpTestingController;

  let configurationService: ConfigurationService;
  let updateUrl: string;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    configurationService = TestBed.inject(ConfigurationService);
    updateUrl = "";
    updateUrl += configurationService.getConfiguration().backend.apiBaseUri;
    updateUrl += configurationService.getConfiguration().backend.apiBasePath;
    updateUrl += configurationService.getConfiguration().backend.apiMappings.user;

    service = TestBed.inject(UserUpdateCommandHandlerService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });

  it("should update user", () => {
    service.execute(userUpdateCommand).subscribe(result => {
      userExpectToBe(result, userUpdateCommandResult);
    });

    const request = httpMock.expectOne(updateUrl);
    expect(request.request.method).toBe("PATCH");
    request.flush(userUpdateCommandResult);

  });
});
