import {Command} from "../../../../../core/cqrs/command/command";

export class UserUpdateCommand implements Command {

  public firstName: string;
  public lastName: string;

}
