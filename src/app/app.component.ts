import {Component, OnInit} from "@angular/core";
import {UserIdleService} from "angular-user-idle";
import {UserSessionService} from "./core/service/user-session/user-session.service";
import {ToastManagerService} from "./core/shared/toast/service/toast-manager.service";
import {Router} from "@angular/router";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent implements OnInit {

  constructor(private userIdle: UserIdleService,
              private router: Router,
              private userSession: UserSessionService,
              private toastManager: ToastManagerService) {
  }

  ngOnInit(): void {
    this.userSessionExpire();
  }

  private userSessionExpire(): void {
    this.userIdle.startWatching();
    this.userIdle.onTimerStart().subscribe(count => console.log(count));
    this.userIdle.onTimeout().subscribe(() => {
      if (this.userSession.isUserLoggedIn()) {
        console.log("User logged out");
        this.userSession.logout();
        this.toastManager.showInfo("You have been logged out - session expired");
        this.router.navigateByUrl("").then(() => {
          this.userIdle.stopTimer();
          this.userIdle.resetTimer();
        });
      }
    });
  }

}
