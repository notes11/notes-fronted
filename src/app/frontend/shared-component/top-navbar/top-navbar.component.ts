import {Component, OnInit} from "@angular/core";
import {environment} from "../../../../environments/environment";
import {Router} from "@angular/router";
import {User} from "../../../backend/model/dto/user";
import {UserSessionService} from "../../../core/service/user-session/user-session.service";
import {ToastManagerService} from "../../../core/shared/toast/service/toast-manager.service";
import {
  UserLogoutCommandHandlerService
} from "../../../backend/user/cqrs/command/user-logout-command/user-logout-command-handler.service";
import {UserLogoutCommand} from "../../../backend/user/cqrs/command/user-logout-command/user-logout-command";
import {ConfigurationService} from "../../../core/service/app-config/configuration.service";

@Component({
  selector: "app-top-navbar",
  templateUrl: "./top-navbar.component.html",
  styleUrls: ["./top-navbar.component.scss"]
})
export class TopNavbarComponent implements OnInit {

  public title: string;
  public appMappings = environment.appMapping;

  private configurationService: ConfigurationService;
  private userSession: UserSessionService;
  private logoutService: UserLogoutCommandHandlerService;
  private router: Router;
  private toastManager: ToastManagerService;

  constructor(configurationService: ConfigurationService,
              router: Router,
              userSession: UserSessionService,
              logoutService: UserLogoutCommandHandlerService,
              toastManager: ToastManagerService) {
    this.configurationService = configurationService;
    this.router = router;
    this.userSession = userSession;
    this.logoutService = logoutService;
    this.toastManager = toastManager;
  }

  ngOnInit(): void {
    this.title = this.configurationService.getConfiguration().title;
  }

  public isActivateWhenUserNotLoggedIn(): boolean {
    return !this.userSession.isUserLoggedIn();
  }

  public isActivateWhenUserLoggedIn(): boolean {
    return this.userSession.isUserLoggedIn();
  }

  public currentLoggedInUser(): User {
    return this.userSession.getLoggedUser();
  }

  public logout(): void {
    this.logoutService.execute(new UserLogoutCommand());
    this.toastManager.showInfo("You have been logged out");
    // noinspection JSIgnoredPromiseFromCall
    this.router.navigateByUrl(environment.appMapping.index);
  }

}
