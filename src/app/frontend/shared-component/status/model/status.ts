import {State} from "./state";

export class Status {
  public state: State = State.ERROR;
  public title: string = "Error";
  public message: string = "Oops, something went wrong...";
}
