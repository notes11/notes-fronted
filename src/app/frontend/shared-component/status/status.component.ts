import {Component, OnInit} from "@angular/core";
import {State} from "./model/state";
import {environment} from "../../../../environments/environment";
import {Status} from "./model/status";

@Component({
  selector: "app-status",
  templateUrl: "./status.component.html",
  styleUrls: ["./status.component.scss"]
})
export class StatusComponent implements OnInit {

  public status: Status;
  public alertTypeClass: string = "danger";
  public homePageLink: string = environment.appMapping.index;

  constructor() {
  }

  ngOnInit(): void {
    this.initStatus();
    this.chooseAlertType();
  }

  private initStatus(): void {
    if (history && history.state) {
      this.status = history.state;
      if (this.status !== null && this.status.state === undefined && this.status.title === undefined &&
        this.status.message === undefined) {
        this.status = new Status();
      }
    } else {
      this.status = new Status();
    }
  }

  private chooseAlertType(): void {
    switch (this.status.state) {
      case State.SUCCESS:
        this.alertTypeClass = "success";
        break;
      case State.ERROR:
        this.alertTypeClass = "danger";
        break;
    }
  }
}
