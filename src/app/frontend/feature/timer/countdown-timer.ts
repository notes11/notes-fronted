import {Timer} from "./timer";

export class CountdownTimer implements Timer {

  private readonly initialNumberOfSeconds: number;
  private interval;
  private secondsLeft: number;

  constructor(secondsLeft: number) {
    this.secondsLeft = secondsLeft;
    this.initialNumberOfSeconds = secondsLeft;
  }

  start(): void {
    this.interval = setInterval(() => {
      if (this.secondsLeft > 0) {
        this.secondsLeft--;
      } else {
        this.pause();
      }
    }, 1000);
  }

  pause(): void {
    clearInterval(this.interval);
  }

  stop(): void {
    this.pause();
    this.secondsLeft = this.initialNumberOfSeconds;
  }

  reset(): void {
    this.secondsLeft = this.initialNumberOfSeconds;
  }

}
