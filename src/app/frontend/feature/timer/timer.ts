export interface Timer {
  start(): void;

  pause(): void;

  stop(): void;

  reset(): void;
}
