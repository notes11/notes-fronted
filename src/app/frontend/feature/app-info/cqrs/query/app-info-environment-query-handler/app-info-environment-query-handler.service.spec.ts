import {TestBed} from "@angular/core/testing";

import {AppInfoEnvironmentQueryHandlerService} from "./app-info-environment-query-handler.service";
import {AppInfoEnvironmentQuery} from "./app-info-environment-query";
import {HttpClientTestingModule} from "@angular/common/http/testing";

describe("AppInfoEnvironmentQueryHandlerService", () => {
  let service: AppInfoEnvironmentQueryHandlerService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(AppInfoEnvironmentQueryHandlerService);
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });

  it("should get environment name", () => {
    expect(service.execute(new AppInfoEnvironmentQuery())).toBeTruthy();
    expect(service.execute(new AppInfoEnvironmentQuery()).environment).toBe("local");
  });
});
