import {Injectable} from "@angular/core";
import {QueryHandler} from "../../../../../../core/cqrs/query/handler/query-handler";
import {AppInfoEnvironmentQuery} from "./app-info-environment-query";
import {AppInfoEnvironmentQueryResult} from "./app-info-environment-query-result";
import {ConfigurationService} from "../../../../../../core/service/app-config/configuration.service";

@Injectable({
  providedIn: "root"
})
export class AppInfoEnvironmentQueryHandlerService implements QueryHandler<AppInfoEnvironmentQuery, AppInfoEnvironmentQueryResult> {

  constructor(private configurationService: ConfigurationService) {
  }

  execute(query: AppInfoEnvironmentQuery): | AppInfoEnvironmentQueryResult {
    return {
      environment: this.configurationService.getConfiguration().environment
    };
  }
}
