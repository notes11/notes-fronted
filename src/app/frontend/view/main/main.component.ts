import {Component, OnInit} from "@angular/core";
import {environment} from "../../../../environments/environment";
import {UserSessionService} from "../../../core/service/user-session/user-session.service";
import {ConfigurationService} from "../../../core/service/app-config/configuration.service";

@Component({
  selector: "app-index",
  templateUrl: "./main.component.html",
  styleUrls: ["./main.component.scss"]
})
export class MainComponent implements OnInit {

  public title: string;

  public loginLink: string = environment.appMapping.user.login;
  public registerLink: string = environment.appMapping.user.register;

  constructor(public userSession: UserSessionService,
              private configurationService: ConfigurationService) {
    this.userSession = userSession;
  }

  ngOnInit(): void {
    this.title = this.configurationService.getConfiguration().title;
  }

}
