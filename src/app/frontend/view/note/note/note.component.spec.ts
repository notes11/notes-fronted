import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import { ComponentFixture, TestBed } from "@angular/core/testing";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { RouterTestingModule } from "@angular/router/testing";

import { NoteComponent } from "./note.component";
import {appRoutes} from "../../../../routes";
import {
  note,
  noteAddCommand,
  noteAddCommandResult, noteDeleteCommandResult,
  notePageEmptyResponse,
  notePageResponse, noteUpdateCommand, noteUpdateCommandResult
} from "../../../../../test/data/test-data.spec";
import {ConfigurationService} from "../../../../core/service/app-config/configuration.service";
import {ActivatedRoute, Router} from "@angular/router";
import {ToastManagerService} from "../../../../core/shared/toast/service/toast-manager.service";
import {of} from "rxjs";

describe("NoteComponent", () => {
  // noinspection DuplicatedCode
  let component: NoteComponent;
  let fixture: ComponentFixture<NoteComponent>;
  let httpMock: HttpTestingController;
  let router: Router;
  let configuration: ConfigurationService;
  let noteUrlBackend: string = "";

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, FormsModule, HttpClientTestingModule, RouterTestingModule.withRoutes(appRoutes)],
      declarations: [ NoteComponent ]
    }).compileComponents();
    // noinspection DuplicatedCode
    configuration = TestBed.inject(ConfigurationService);
    noteUrlBackend = "";
    noteUrlBackend += configuration.getConfiguration().backend.apiBaseUri;
    noteUrlBackend += configuration.getConfiguration().backend.apiBasePath;
    noteUrlBackend += configuration.getConfiguration().backend.apiMappings.note;

    router = TestBed.inject(Router);
    httpMock = TestBed.inject(HttpTestingController);
    fixture = TestBed.createComponent(NoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    httpMock.verify();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should create - default values", () => {
    expect(component).toBeTruthy();
    expect(component.isLoading).toBeFalsy();
    expect(component.isEditing).toBeFalsy();
  });

  it("should submit new note", () => {
    const navigateSpy = spyOn(router, "navigateByUrl");

    expect(component.noteFormGroup.valid).toBeFalsy();
    expect(component.isEditing).toBeFalsy();

    component.noteFormGroup.controls.title.setValue(noteAddCommand.title);
    component.noteFormGroup.controls.noteText.setValue(noteAddCommand.noteText);
    component.onSubmit();

    const request = httpMock.expectOne(noteUrlBackend);
    expect(request.request.method).toBe("POST");
    request.flush(noteAddCommandResult);

    expect(navigateSpy).toHaveBeenCalledOnceWith(`//${noteAddCommandResult.uuid}`);
  });

  it("should submit new note then receive error", () => {
    const toastManager = TestBed.inject(ToastManagerService);

    component.noteFormGroup.controls.title.setValue(noteAddCommand.title);
    component.noteFormGroup.controls.noteText.setValue(noteAddCommand.noteText);
    component.onSubmit();

    const request = httpMock.expectOne(noteUrlBackend);
    expect(request.request.method).toBe("POST");
    request.flush(noteAddCommandResult, {
      status: 500,
      statusText: "Internal Server Error"
    });

    expect(toastManager.toasts.length).toBeGreaterThanOrEqual(1);
  });
});

describe("NoteComponent with uuid", () => {
  // noinspection DuplicatedCode
  let component: NoteComponent;
  let fixture: ComponentFixture<NoteComponent>;
  let httpMock: HttpTestingController;
  let router: Router;
  let configuration: ConfigurationService;
  let noteUrlBackend: string = "";
  let toastManager: ToastManagerService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, FormsModule, HttpClientTestingModule, RouterTestingModule.withRoutes(appRoutes)],
      declarations: [NoteComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            params: of({
              uuid: noteAddCommandResult.uuid
            })
          }
        }
      ]
    }).compileComponents();
    // noinspection DuplicatedCode
    configuration = TestBed.inject(ConfigurationService);
    noteUrlBackend = "";
    noteUrlBackend += configuration.getConfiguration().backend.apiBaseUri;
    noteUrlBackend += configuration.getConfiguration().backend.apiBasePath;
    noteUrlBackend += configuration.getConfiguration().backend.apiMappings.note;
    noteUrlBackend += `?size=1&page=0&direction=DESC&uuid=${note.uuid}`;

    toastManager = TestBed.inject(ToastManagerService);
    router = TestBed.inject(Router);
    httpMock = TestBed.inject(HttpTestingController);
    fixture = TestBed.createComponent(NoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    httpMock.verify();
  });

  it("should create with uuid", () => {
    const request = httpMock.expectOne(noteUrlBackend);
    expect(request.request.method).toBe("GET");
    request.flush(notePageResponse);

    expect(component).toBeDefined();
    expect(component.isEditing).toBeTruthy();
  });

  it("should create with uuid - not found note", () => {
    const request = httpMock.expectOne(noteUrlBackend);
    expect(request.request.method).toBe("GET");
    request.flush(notePageEmptyResponse);

    expect(component).toBeDefined();
    expect(component.isEditing).toBeFalsy();
    expect(component.noteFormGroup.valid).toBeFalsy();
  });

  it("should create update note successfully", () => {
    // given
    const requestInit = httpMock.expectOne(noteUrlBackend);
    expect(requestInit.request.method).toBe("GET");
    requestInit.flush(notePageResponse);

    expect(component).toBeDefined();
    expect(component.isEditing).toBeTruthy();
    expect(component.noteFormGroup.valid).toBeFalsy();

    // when
    component.noteFormGroup.controls.title.setValue(noteUpdateCommand.title);
    component.noteFormGroup.controls.noteText.setValue(noteUpdateCommand.noteText);
    expect(component.noteFormGroup.valid).toBeTruthy();
    component.onSubmit();

    let noteUrlBackendUpdate = "";
    noteUrlBackendUpdate += configuration.getConfiguration().backend.apiBaseUri;
    noteUrlBackendUpdate += configuration.getConfiguration().backend.apiBasePath;
    noteUrlBackendUpdate += configuration.getConfiguration().backend.apiMappings.note;
    noteUrlBackendUpdate += `/${noteUpdateCommandResult.uuid}`;

    const requestUpdate = httpMock.expectOne(noteUrlBackendUpdate);
    expect(requestUpdate.request.method).toBe("PUT");
    requestUpdate.flush(noteUpdateCommandResult);


    // then
    expect(toastManager.toasts.length).toBeGreaterThanOrEqual(1);
  });

  it("should fail on note update", () => {
    // given
    const requestInit = httpMock.expectOne(noteUrlBackend);
    expect(requestInit.request.method).toBe("GET");
    requestInit.flush(notePageResponse);

    expect(component).toBeDefined();
    expect(component.isEditing).toBeTruthy();
    expect(component.noteFormGroup.valid).toBeFalsy();

    // when
    component.noteFormGroup.controls.title.setValue(noteUpdateCommand.title);
    component.noteFormGroup.controls.noteText.setValue(noteUpdateCommand.noteText);
    expect(component.noteFormGroup.valid).toBeTruthy();
    component.onSubmit();

    let noteUrlBackendUpdate = "";
    noteUrlBackendUpdate += configuration.getConfiguration().backend.apiBaseUri;
    noteUrlBackendUpdate += configuration.getConfiguration().backend.apiBasePath;
    noteUrlBackendUpdate += configuration.getConfiguration().backend.apiMappings.note;
    noteUrlBackendUpdate += `/${noteUpdateCommandResult.uuid}`;

    const requestUpdate = httpMock.expectOne(noteUrlBackendUpdate);
    expect(requestUpdate.request.method).toBe("PUT");
    requestUpdate.flush(noteUpdateCommandResult, {
      status: 500,
      statusText: "Internal Server Error"
    });


    // then
    expect(toastManager.toasts.length).toBeGreaterThanOrEqual(1);
    expect(toastManager.toasts[0].body).toBe("Failed to update a note. Please try again later.");
  });

  it("should delete note", () => {
    const request = httpMock.expectOne(noteUrlBackend);
    expect(request.request.method).toBe("GET");
    request.flush(notePageResponse);

    component.deleteNote(notePageResponse);

    let noteDeleteUrlBackend = configuration.getConfiguration().backend.apiBaseUri;
    noteDeleteUrlBackend += configuration.getConfiguration().backend.apiBasePath;
    noteDeleteUrlBackend += configuration.getConfiguration().backend.apiMappings.note;
    noteDeleteUrlBackend += `/${notePageResponse.content[0].uuid}`;
    const deleteRequest = httpMock.expectOne(noteDeleteUrlBackend);
    expect(deleteRequest.request.method).toBe("DELETE");
    deleteRequest.flush(noteDeleteCommandResult);

    expect(toastManager.toasts.length).toBeGreaterThanOrEqual(1);
    expect(toastManager.toasts[0].body).toBe("Note has been successfully deleted.");
  });

  it("should get error on delete note", () => {
    const request = httpMock.expectOne(noteUrlBackend);
    expect(request.request.method).toBe("GET");
    request.flush(notePageResponse);

    component.deleteNote(notePageResponse);

    let noteDeleteUrlBackend = configuration.getConfiguration().backend.apiBaseUri;
    noteDeleteUrlBackend += configuration.getConfiguration().backend.apiBasePath;
    noteDeleteUrlBackend += configuration.getConfiguration().backend.apiMappings.note;
    noteDeleteUrlBackend += `/${notePageResponse.content[0].uuid}`;
    const deleteRequest = httpMock.expectOne(noteDeleteUrlBackend);
    expect(deleteRequest.request.method).toBe("DELETE");
    deleteRequest.flush(noteDeleteCommandResult, {
      status: 500,
      statusText: "Internal Server Error"
    });

    expect(toastManager.toasts.length).toBeGreaterThanOrEqual(1);
    expect(toastManager.toasts[0].body).toBe("Failed to delete a note. Please try again later.");
  });
});
