import {Component, OnDestroy, OnInit} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {environment} from "../../../../../environments/environment";
import {Subscription} from "rxjs";
import {Router} from "@angular/router";
import {State} from "../../../shared-component/status/model/state";
import {Status} from "../../../shared-component/status/model/status";
import {ToastManagerService} from "../../../../core/shared/toast/service/toast-manager.service";
import {
  UserRegisterCommandHandlerService
} from "../../../../backend/user/cqrs/command/user-register-command/user-register-command-handler.service";
import {UserRegisterCommand} from "../../../../backend/user/cqrs/command/user-register-command/user-register-command";

@Component({
  selector: "app-create-user",
  templateUrl: "./user-register.component.html",
  styleUrls: ["./user-register.component.scss"]
})
export class UserRegisterComponent implements OnInit, OnDestroy {

  public userCreateForm: FormGroup;
  public submitted: boolean = false;
  public loading: boolean = false;
  public errorMessage: string;
  public statusMapping = environment.appMapping.status;

  private router: Router;
  private formBuilder: FormBuilder;
  private user: UserRegisterCommand = new UserRegisterCommand();
  private registerService: UserRegisterCommandHandlerService;
  private subscription: Subscription;
  private toastManagerService: ToastManagerService;

  constructor(router: Router,
              formBuilder: FormBuilder,
              registerService: UserRegisterCommandHandlerService,
              toastManagerService: ToastManagerService) {
    this.router = router;
    this.formBuilder = formBuilder;
    this.registerService = registerService;
    this.toastManagerService = toastManagerService;
  }

  ngOnInit(): void {
    this.initValidationRulesForForm();
    this.initFormState();
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  public onSubmit(userCreateForm: FormGroup): void {
    this.submitted = true;
    if (userCreateForm.valid) {
      this.user.username = userCreateForm.value.username;
      this.user.password = userCreateForm.value.password;
      this.user.addressEmail = userCreateForm.value.addressEmail;

      this.loading = true;
      this.subscription = this.registerService.execute(this.user).subscribe(() => {
          this.handleStatus(State.SUCCESS, "Success", "Account created successfully");
          this.toastManagerService.showSuccess("Successfully created account");
        },
        error => {
          this.errorMessage = error.errorMessage;

          this.toastManagerService.showDanger("Filed to create account");
        }, () => {
          this.loading = false;
        });
    }
  }

  private handleStatus(state: State, title: string, message: string): void {
    const status: Status = new Status();
    status.state = state;
    status.title = title;
    status.message = message;
    // noinspection JSIgnoredPromiseFromCall
    this.router.navigateByUrl(this.statusMapping, {state: status});
  }

  private initValidationRulesForForm(): void {
    this.userCreateForm = this.formBuilder.group({
      username: [this.user.username, [Validators.required, Validators.minLength(5), Validators.maxLength(50)]],
      addressEmail: [this.user.addressEmail, [Validators.required, Validators.maxLength(100),
        Validators.pattern("(^[A-z0-9]+)(@)([A-z]+)(\.)([A-z]+$)")]],
      password: [this.user.password, [Validators.required, Validators.minLength(5), Validators.maxLength(80)]],
      passwordRepeat: [this.user.passwordRepeat, [Validators.required, Validators.minLength(5), Validators.maxLength(80)]]
    });
  }

  private initFormState(): void {
    this.submitted = false;
    this.loading = false;
  }

}
