import {ComponentFixture, TestBed} from "@angular/core/testing";

import {UserProfileComponent} from "./user-profile.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {DebugElement} from "@angular/core";
import {User} from "../../../../backend/model/dto/user";
import {UserSessionService} from "../../../../core/service/user-session/user-session.service";
import {ToastManagerService} from "../../../../core/shared/toast/service/toast-manager.service";
import {
  UserUpdateCommandResult
} from "../../../../backend/user/cqrs/command/user-update-command/user-update-command-result";
import {ConfigurationService} from "../../../../core/service/app-config/configuration.service";

describe("ViewProfileComponent", () => {
  let component: UserProfileComponent;
  let fixture: ComponentFixture<UserProfileComponent>;
  let debugElement: DebugElement;
  let userSessionService: UserSessionService;
  let toastManager: ToastManagerService;
  let httpMock: HttpTestingController;

  let configurationService: ConfigurationService;
  let updateUrl: string;

  const user: User = {
    uuid: "uuid",
    createdBy: "test",
    createdTimestamp: new Date(Date.now()),
    modifiedBy: null,
    modificationTimestamp: null,
    username: "username",
    firstName: "firstName",
    lastName: "lastName",
    addressEmail: "test@gmail.com",
    isActive: true,
    roles: [],
    jwtToken: ""
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, FormsModule, HttpClientTestingModule, HttpClientTestingModule],
      declarations: [UserProfileComponent]
    }).compileComponents();
    configurationService = TestBed.inject(ConfigurationService);
    updateUrl = "";
    updateUrl += configurationService.getConfiguration().backend.apiBaseUri;
    updateUrl += configurationService.getConfiguration().backend.apiBasePath;
    updateUrl += configurationService.getConfiguration().backend.apiMappings.user;

    httpMock = TestBed.inject(HttpTestingController);
    toastManager = TestBed.inject(ToastManagerService);
    userSessionService = TestBed.inject(UserSessionService);
    userSessionService.login(user);
    fixture = TestBed.createComponent(UserProfileComponent);
    component = fixture.componentInstance;
    debugElement = fixture.debugElement;
    fixture.detectChanges();
  });

  afterEach(async () => {
    userSessionService.logout();
    httpMock.verify();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should invoke `onReset` correctly", () => {
    component.onReset();
    expect(component.userProfileFormGroup.controls.firstName.value).toEqual(user.firstName);
    expect(component.userProfileFormGroup.controls.lastName.value).toEqual(user.lastName);
  });

  it("should enable editing", () => {
    // given
    const editButton: HTMLElement = debugElement.nativeElement.querySelector("div>div>a[role='button']");
    expect(editButton).toBeTruthy();
    expect(editButton.innerText).toBe("Edit");

    // when
    editButton.click();

    // then
    expect(component.isDisabled).toBeFalsy();
  });

  it("should not submit on invalid form", () => {
    // given
    const toLongString = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
    component.userProfileFormGroup.controls.firstName.setValue(toLongString);
    component.userProfileFormGroup.controls.lastName.setValue(toLongString);
    component.userProfileFormGroup.updateValueAndValidity();
    expect(component.userProfileFormGroup.invalid).toBeTruthy();
    // when
    component.onSubmit(component.userProfileFormGroup);
    //then
    expect(toastManager.toasts.length).toEqual(0);
    expect(component.userProfileFormGroup.get("firstName").hasError("maxlength"));
    expect(component.userProfileFormGroup.get("lastName").hasError("maxlength"));
  });

  it("should update user first name and last name", () => {
    // given
    const newFirstName: string = "New first name";
    const newLastName: string = "New last name";
    component.userProfileFormGroup.controls.firstName.setValue(newFirstName);
    component.userProfileFormGroup.controls.lastName.setValue(newLastName);
    expect(component.userProfileFormGroup.valid).toBeTruthy();

    const userLoginCommandExpectedResult: UserUpdateCommandResult = {
      uuid: "uuid",
      createdBy: "test",
      createdTimestamp: new Date(Date.now()),
      modifiedBy: null,
      modificationTimestamp: null,
      username: "username",
      firstName: newFirstName,
      lastName: newLastName,
      addressEmail: "test@gmail.com",
      isActive: true,
      roles: [],
      jwtToken: ""
    };

    // when
    component.onSubmit(component.userProfileFormGroup);

    const request = httpMock.expectOne(updateUrl);
    expect(request.request.method).toBe("PATCH");
    request.flush(userLoginCommandExpectedResult);
    // then
    expect(toastManager.toasts.length).toEqual(1);
    expect(userSessionService.getLoggedUser().firstName).toEqual(newFirstName);
    expect(userSessionService.getLoggedUser().lastName).toEqual(newLastName);
  });

  it("should fail to update", () => {
    // given
    const newFirstName: string = "New first name";
    const newLastName: string = "New last name";
    component.userProfileFormGroup.controls.firstName.setValue(newFirstName);
    component.userProfileFormGroup.controls.lastName.setValue(newLastName);
    expect(component.userProfileFormGroup.valid).toBeTruthy();

    const userLoginCommandExpectedResult: UserUpdateCommandResult = {
      uuid: "uuid",
      createdBy: "test",
      createdTimestamp: new Date(Date.now()),
      modifiedBy: null,
      modificationTimestamp: null,
      username: "username",
      firstName: newFirstName,
      lastName: newLastName,
      addressEmail: "test@gmail.com",
      isActive: true,
      roles: [],
      jwtToken: ""
    };

    // when
    component.onSubmit(component.userProfileFormGroup);

    const request = httpMock.expectOne(updateUrl);
    expect(request.request.method).toBe("PATCH");
    request.flush(userLoginCommandExpectedResult, {
      status: 401,
      statusText: "Unauthorized"
    });
    // then
    expect(toastManager.toasts.length).toEqual(1);
    expect(toastManager.toasts[0].header).toEqual("Error");
  });
});
