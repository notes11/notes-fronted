import {Component, OnDestroy, OnInit} from "@angular/core";
import {User} from "../../../../backend/model/dto/user";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Subscription} from "rxjs";
import {ToastManagerService} from "../../../../core/shared/toast/service/toast-manager.service";
import {UserSessionService} from "../../../../core/service/user-session/user-session.service";
import {
  UserUpdateCommandHandlerService
} from "../../../../backend/user/cqrs/command/user-update-command/user-update-command-handler.service";

@Component({
  selector: "app-view-profile",
  templateUrl: "./user-profile.component.html",
  styleUrls: ["./user-profile.component.scss"]
})
export class UserProfileComponent implements OnInit, OnDestroy {

  public currentlyLoggedInUser: User;
  public isDisabled = true;
  public userProfileFormGroup: FormGroup;
  public loading: boolean = false;

  private userSession: UserSessionService;
  private formBuilder: FormBuilder;
  private updateService: UserUpdateCommandHandlerService;
  private subscription: Subscription;
  private toastManager: ToastManagerService;

  constructor(userSession: UserSessionService,
              formBuilder: FormBuilder,
              updateService: UserUpdateCommandHandlerService,
              toastManager: ToastManagerService) {
    this.userSession = userSession;
    this.formBuilder = formBuilder;
    this.updateService = updateService;
    this.toastManager = toastManager;
  }

  ngOnInit(): void {
    this.currentlyLoggedInUser = this.userSession.getLoggedUser();
    this.createForm();
  }

  public switchDisabled(): void {
    this.isDisabled = !this.isDisabled;
  }

  public onSubmit(userProfileFormGroup: FormGroup): void {
    if (userProfileFormGroup.invalid) {
      return;
    } else {
      const userToUpdate: User = this.currentlyLoggedInUser;
      userToUpdate.firstName = userProfileFormGroup.controls.firstName.value;
      userToUpdate.lastName = userProfileFormGroup.controls.lastName.value;

      this.loading = true;
      this.subscription = this.updateService.execute(userToUpdate)
        .subscribe(user => {
            this.currentlyLoggedInUser = user;
            this.userSession.refresh(user);
            this.toastManager.showSuccess("User has been updated");
          },
          () => {
            this.toastManager.showDanger("Failed to update user");
          },
          () => {
            this.loading = false;
          });
    }
  }

  public onReset(): void {
    this.userProfileFormGroup.controls.firstName.setValue(this.currentlyLoggedInUser.firstName);
    this.userProfileFormGroup.controls.lastName.setValue(this.currentlyLoggedInUser.lastName);
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  private createForm(): void {
    this.userProfileFormGroup = this.formBuilder.group({
      firstName: [this.currentlyLoggedInUser.firstName, [Validators.minLength(1), Validators.maxLength(50)]],
      lastName: [this.currentlyLoggedInUser.lastName, [Validators.minLength(1), Validators.maxLength(50)]]
    });
  }

}
