import {User} from "../../app/backend/model/dto/user";
import {Params} from "@angular/router";
import {Page} from "../../app/backend/model/other/page";
import {UserLoginCommand} from "../../app/backend/user/cqrs/command/user-login-command/user-login-command";
import {UserLoginCommandResult} from "../../app/backend/user/cqrs/command/user-login-command/user-login-command-result";
import {UserUpdateCommand} from "../../app/backend/user/cqrs/command/user-update-command/user-update-command";
import {
  UserUpdateCommandResult
} from "../../app/backend/user/cqrs/command/user-update-command/user-update-command-result";
import {UserRegisterCommand} from "../../app/backend/user/cqrs/command/user-register-command/user-register-command";
import {
  UserRegisterCommandResult
} from "../../app/backend/user/cqrs/command/user-register-command/user-register-command-result";
import {NoteAddCommand} from "../../app/backend/note/cqrs/command/add-command-handler/note-add-command";
import {NoteAddCommandResult} from "../../app/backend/note/cqrs/command/add-command-handler/note-add-command-result";
import {NoteUpdateCommand} from "../../app/backend/note/cqrs/command/note-update/note-update-command";
import {NoteUpdateCommandResult} from "../../app/backend/note/cqrs/command/note-update/note-update-command-result";
import {Note} from "../../app/backend/model/dto/note";
import {NoteDeleteCommand} from "../../app/backend/note/cqrs/command/note-delete/note-delete-command";
import {NoteDeleteCommandResult} from "../../app/backend/note/cqrs/command/note-delete/note-delete-command-result";

export const user: User = {
  uuid: "b9601192-48c5-11ec-81d3-0242ac130003",
  createdBy: "unit-test",
  createdTimestamp: new Date(Date.now()),
  modifiedBy: null,
  modificationTimestamp: null,
  username: "test-user",
  firstName: "Jan",
  lastName: "Kowalski",
  addressEmail: "jk@gmail.com",
  isActive: true,
  roles: [],
  jwtToken: "token"
};

export const userRegisterCommand: UserRegisterCommand = {
  username: user.username,
  addressEmail: user.addressEmail,
  password: "test-password",
  passwordRepeat: "test-password"
};

export const userRegisterCommandResult: UserRegisterCommandResult = {
  uuid: user.uuid,
  createdBy: user.createdBy,
  createdTimestamp: user.createdTimestamp,
  modifiedBy: user.modifiedBy,
  modificationTimestamp: user.modificationTimestamp,
  username: user.username,
  firstName: user.firstName,
  lastName: user.lastName,
  addressEmail: user.addressEmail,
  isActive: user.isActive,
  roles: user.roles,
  jwtToken: user.jwtToken
};

export const userLoginCommand: UserLoginCommand = {
  username: user.username,
  password: userRegisterCommand.password
};

export const userLoginCommandResult: UserLoginCommandResult = {
  uuid: user.uuid,
  createdBy: user.createdBy,
  createdTimestamp: user.createdTimestamp,
  modifiedBy: user.modifiedBy,
  modificationTimestamp: user.modificationTimestamp,
  username: user.username,
  firstName: user.firstName,
  lastName: user.lastName,
  addressEmail: user.addressEmail,
  isActive: user.isActive,
  roles: user.roles,
  jwtToken: user.jwtToken
};

export const userUpdateCommand: UserUpdateCommand = {
  firstName: "new-firstName",
  lastName: "new-lastName"
};

export const userUpdateCommandResult: UserUpdateCommandResult = {
  uuid: user.uuid,
  createdBy: user.createdBy,
  createdTimestamp: user.createdTimestamp,
  modifiedBy: user.modifiedBy,
  modificationTimestamp: user.modificationTimestamp,
  username: user.username,
  firstName: userUpdateCommand.firstName,
  lastName: userUpdateCommand.lastName,
  addressEmail: user.addressEmail,
  isActive: user.isActive,
  roles: user.roles,
  jwtToken: user.jwtToken
};

export const note: Note = {
  uuid: "0439d158-f08f-4189-9a20-726a8f84d3fa",
  createdBy: user.createdBy,
  createdTimestamp: new Date(Date.now()),
  modifiedBy: null,
  modificationTimestamp: null,
  title: "Title of new note",
  noteText: "Title of new note",
  owner: user
};

export const noteAddCommand: NoteAddCommand = {
  title: note.title,
  noteText: note.noteText
};

export const noteAddCommandResult: NoteAddCommandResult = {
  uuid: note.uuid,
  createdBy: note.createdBy,
  createdTimestamp: note.createdTimestamp,
  modifiedBy: note.modifiedBy,
  modificationTimestamp: note.modificationTimestamp,
  title: noteAddCommand.title,
  noteText: noteAddCommand.noteText,
  owner: note.owner
};

export const noteUpdateCommand: NoteUpdateCommand = {
  uuid: noteAddCommandResult.uuid,
  title: "Some tile",
  noteText: "Some note"
};

export const noteUpdateCommandResult: NoteUpdateCommandResult = {
  uuid: noteUpdateCommand.uuid,
  createdBy: user.createdBy,
  createdTimestamp: new Date(),
  modifiedBy: user.createdBy,
  modificationTimestamp: new Date(),
  title: noteUpdateCommand.title,
  noteText: noteUpdateCommand.noteText,
  owner: note.owner
};

export const noteDeleteCommand: NoteDeleteCommand = {
  uuid: noteAddCommandResult.uuid
};

export const noteDeleteCommandResult: NoteDeleteCommandResult = {
};

export const pageParams: Params = {
  _page: 1,
  _size: 10,
  _direction: "ASC"
};

export const pageResponse: Page<any> = {
  content: [],
  first: false,
  last: false,
  // eslint-disable-next-line id-blacklist
  number: 1,
  size: 10,
  totalPages: 10,
  numberOfElements: 10,
  totalElements: 100
};

export const notePageResponse: Page<Note> = {
  content: [note, note, note, note, note],
  first: false,
  last: false,
  // eslint-disable-next-line id-blacklist
  number: 0,
  size: 10,
  totalPages: 10,
  numberOfElements: 10,
  totalElements: 100
};

export const notePageResponseSize2: Page<Note> = {
  content: [note, note],
  first: false,
  last: false,
  // eslint-disable-next-line id-blacklist
  number: 0,
  size: 2,
  totalPages: 10,
  numberOfElements: 10,
  totalElements: 100
};

export const notePageEmptyResponse: Page<Note> = {
  content: [],
  first: false,
  last: false,
  // eslint-disable-next-line id-blacklist
  number: 1,
  size: 10,
  totalPages: 10,
  numberOfElements: 10,
  totalElements: 100
};
