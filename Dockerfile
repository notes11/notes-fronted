FROM nginx:alpine
COPY default.conf /etc/nginx/conf.d/default.conf
COPY dist/notes-frontend /usr/share/nginx/html
